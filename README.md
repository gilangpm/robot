##Toy Robot Simulator

###Description:
- The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
- There are no other obstructions on the table surface.
- The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement that would result in the robot falling from the table must be prevented, however further valid movement commands must still
be alowed.

### Usage
`ruby -r ./lib/robot.rb -e "Robot.command(1)"`

### Allowed Commands
`PLACE,X,Y,F` Placing the robot X[integer], Y[integer], F[(NORTH|EAST|SOUTH|WEST)]
`MOVE` Move the robot forward
`LEFT`or `RIGHT` Rotate the robot 90 degrees in the specified direction without changing the position of the robot.
`REPORT` Will announce the X,Y and F of the robot.

### Test
`rspec`

### Alternate Usage (using irb console)
    require './lib/robot'
    robot = Robot.new
    robot.command("PLACE,0,1,EAST")
    robot.command("MOVE")
    robot.command("LEFT")
    robot.command("REPORT")
