class Robot
  COMMANDS = ["MOVE", "LEFT", "RIGHT", "REPORT"]

  def self.command(console = nil)
    if console == 1
      @flag = true
    end
    if @robot.nil?
      @robot = new
    else
      @robot = @robot
    end
    print "Write your command\n"
    
    input = gets.strip
    @robot.command(input)
  end

  def self.call_console
    if @flag == true
      Robot.command(1)
    end
  end

  def command(input = nil)
    if validates_command?(input)
      execute_command(input)
    else
      print "Invalid Command!\n"
      Robot.call_console
    end
  end

  protected
  def placing(input)
    unless @x.nil? && @y.nil? && @f.nil?
      print "Robot has been placed\n"
    else
      @x = input.split(',')[1].to_i
      @y = input.split(',')[2].to_i
      @f = input.split(',')[3]
      print "Robot Placed!\n"
    end
  end

  def execute_command(input)
    if (/PLACE,[0-9],[0-9],(NORTH|EAST|WEST|SOUTH)/.match(input))
      placing(input)
      
    else
      if has_position?
        case input
        when "MOVE"
          move
        when "REPORT"
          report
        when "LEFT"
          left
        when "RIGHT"
          right  
        end 
      else
        print "Please set the place first!\n"
      end
    end
    Robot.call_console
  end

  def move
    if @x > 4 || @y > 4
      print "Robot's position is out of the table\n"
    else
      case @f
      when "NORTH"
        @y += 1 unless @y >= 4
      when "EAST"
        @x += 1 unless @x >= 4
      when "SOUTH"
        @y -= 1 unless @y <= 0
      when "WEST"
        @x -= 1 unless @x <= 0
      end
    end
  end

  def report
    print "#{@x},#{@y},#{@f}\n"
  end
  
  def left
    case @f
    when "NORTH"
      @f = "WEST"
    when "EAST"
      @f = "NORTH"
    when "SOUTH"
      @f = "EAST"
    when "WEST"
      @f = "SOUTH"
    end
  end

  def right
    case @f
    when "NORTH"
      @f = "EAST"
    when "EAST"
      @f = "SOUTH"
    when "SOUTH"
      @f = "WEST"
    when "WEST"
      @f = "NORTH"
    end
  end

  def has_position?
    return true unless @x.nil? || @y.nil? || @f.nil?
  end

  def validates_command?(command)
    COMMANDS.include?(command) ||  (/PLACE,[0-9],[0-9],(NORTH|EAST|WEST|SOUTH)/.match(command))
  end
end