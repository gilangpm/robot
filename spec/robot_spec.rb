require 'robot'
require 'spec_helper'
describe "Robot" do
  it "can be placed on the table" do
    output = capture_stdout{Robot.new.command("PLACE,0,1,NORTH")}
    expect(output).to eq("Robot Placed!\n")
  end

  it "has default position" do
    robot = Robot.new
    robot.command("PLACE,0,1,NORTH")
    expect(robot.instance_variable_get(:@x)).to eq(0)
    expect(robot.instance_variable_get(:@y)).to eq(1)
    expect(robot.instance_variable_get(:@f)).to eq("NORTH")
  end

  it "can be moved forward" do
    robot = Robot.new
    robot.command("PLACE,0,1,NORTH")
    robot.command("MOVE")
    expect(robot.instance_variable_get(:@y)).to eq(2)
  end

  it "reject placing if had placed" do
    robot = Robot.new
    robot.command("PLACE,0,1,NORTH")
    robot.command("MOVE")
    place = capture_stdout {robot.command("PLACE,0,1,EAST")}
    expect(place).to eq("Robot has been placed\n")
  end

  it "can reports the position" do
    robot = Robot.new
    robot.command("PLACE,0,1,NORTH")
    robot.command("MOVE")
    robot.command("MOVE")
    report = capture_stdout {robot.command("REPORT")}
    expect(report).to eq("0,3,NORTH\n")
  end

  it "can be left rotated" do
    robot = Robot.new
    robot.command("PLACE,0,1,NORTH")
    robot.command("LEFT")
    result = capture_stdout{robot.command("REPORT")}
    expect(result).to eq("0,1,WEST\n")
  end

  it "can be right rotated" do
    robot = Robot.new
    robot.command("PLACE,0,1,NORTH")
    robot.command("RIGHT")
    result = capture_stdout{robot.command("REPORT")}
    expect(result).to eq("0,1,EAST\n")
  end

  it "can only receives valid command" do
    robot = Robot.new
    result = capture_stdout {robot.command("ANY COMMAND")}
    expect(result).to eq("Invalid Command!\n")
  end

  it "ignores command when out of the table" do
    robot = Robot.new
    robot.command("PLACE,9,7,EAST")
    result = capture_stdout {robot.command("MOVE")}
    expect(result).to eq("Robot's position is out of the table\n")
  end

  it "can't fall from table" do
    robot = Robot.new
    robot.command("PLACE,0,0,NORTH")
    robot.command("RIGHT")
    robot.command("MOVE")
    robot.command("MOVE")
    robot.command("MOVE")
    robot.command("MOVE")
    robot.command("MOVE")
    result = capture_stdout {robot.command("REPORT")}
    expect(result).to eq("4,0,EAST\n")
  end

  it "does free roam" do
    robot = Robot.new
    robot.command("PLACE,1,2,EAST")
    robot.command("MOVE")
    robot.command("MOVE")
    robot.command("LEFT")
    robot.command("MOVE")
    result = capture_stdout {robot.command("REPORT")}
    expect(result).to eq("3,3,NORTH\n")
  end

  it "can't be moved or rotated if doesn't have position" do
    robot = Robot.new
    result = capture_stdout {robot.command("MOVE")}
    expect(result).to eq("Please set the place first!\n")
  end
end