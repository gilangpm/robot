require 'rspec'
module SpecHelper
  def capture_stdout(&block)
    original_stdout = $stdout
    $stdout = fake = StringIO.new
    begin
      yield
    ensure
      $stdout = original_stdout
    end
    fake.string
  end
end
RSpec.configure do |t|
  t.mock_with :rspec
  t.include SpecHelper
end